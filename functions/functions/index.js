const functions = require('firebase-functions');
const admin = require('firebase-admin')
admin.initializeApp()

exports.sendNotification = functions.region("europe-west2").firestore
    .document('requests/{requestId}').onCreate((snap, context) => {
        const snapData = snap.data();
        const collectorId = snapData.collectorId;
        const citizenId = snapData.citizenId;
        const db = admin.firestore();

        console.log(collectorId);

        db.collection('collectors').where('userId', '==', collectorId).get()
            .then(collectorQuery => {
                collectorQuery.forEach(collectorSnap => {
                    collector = collectorSnap.data()
                    console.log("Collector " + JSON.stringify(collector));
                    db.collection('citizens').where('userId', '==', citizenId).get()
                        .then(citizenQuery => {
                            citizenQuery.forEach(citizenSnap => {
                                citizen = citizenSnap.data()
                                console.log("Citizen " + JSON.stringify(citizen));
                                const citizenFullName = citizen.fullName;
                                const notificationBody = `Dobili ste novi upit od ${citizenFullName}. .`
                                db.collection('users').doc(collectorId).get()
                                    .then(userSnap => {
                                        user = userSnap.data()
                                        console.log(JSON.stringify(user));
                                        const payload = {
											notification: {
												title: 'Dobili ste novi upit.',
												body: notificationBody,
											}
                                        }

                                        admin
                                            .messaging()
                                            .sendToDevice(user.pushToken, payload)
                                            .then(response => {
                                                console.log('Bravo bravo');
                                            })
                                            .catch(error => {
                                                console.log('error sending message: ', error);
                                            })

                                    })
                            });
                        
                        })
                });
            });
    })


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
