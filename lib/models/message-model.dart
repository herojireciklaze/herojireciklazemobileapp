class Message {
  String fromUser;
  String toUser;
  String content;

  Message({this.toUser, this.fromUser, this.content});

  Message.fromMap(Map snapshot) :
        fromUser = snapshot["fromUser"] ?? '',
        toUser = snapshot["toUser"] ?? '',
        content = snapshot["message"] ?? '';

  toJson() {
    return {
      "fromUser": fromUser,
      "toUser": toUser,
      "conent": content
    };
  }
}
