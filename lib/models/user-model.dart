import 'package:heroji_reciklaze_2020/models/user-roles.dart';

class User {
  String userId;
  String fbUid;
  UserRoles role;
  String pushToken;

  User({this.fbUid, this.role, this.pushToken});

  User.fromMap(Map snapshot,String userId) :
        userId = userId ?? '',
        fbUid = snapshot['fbUid'] ?? '',
        role = UserRoles.values.firstWhere((e) => e.toString().split(".")[1] == snapshot['role']),
        pushToken = snapshot['pushToken'];


  toJson() {
    return {
      "userId": userId,
      "fbUid": fbUid,
      "role": getUserRole[role],
      "pushToken": pushToken
    };
  }
}