import 'package:heroji_reciklaze_2020/models/user-model.dart';


class Collector {
  User user;
  String collectorId;
  String userId;
  String fullName;
  String username;
  String phoneNumber;
  String about;
  String address;
  bool approvedByAdmin;
  List<String> type;

  Collector({this.user, this.userId, this.type, this.phoneNumber, this.address, this.fullName, this.username, this.about, this.approvedByAdmin});

  Collector.fromMap(Map snapshot,String collectorId) :
        collectorId = collectorId,
        userId = snapshot['userId'] ?? '',
        fullName = snapshot['fullName'] ?? '',
        username = snapshot['username'] ?? '',
        phoneNumber = snapshot['phoneNumber'] ?? '',
        about = snapshot['about'] ?? '',
        address = snapshot['address'] ?? '',
        approvedByAdmin = snapshot['approvedByAdmin'] ?? false,
        type = List.from(snapshot['type']) ?? [];

  toJson() {
    return {
      "userId": userId,
      "fullName": fullName,
      "username": username,
      "phoneNumber": phoneNumber,
      "about": about,
      "address": address,
      "approvedByAdmin":approvedByAdmin,
      "type" : type,
      "userId": userId
    };
  }
}
