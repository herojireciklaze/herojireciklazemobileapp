import 'package:heroji_reciklaze_2020/models/user-model.dart';


class Citizen{
  User user;
  String userId;
  String fullName;
  String username;
  String phoneNumber;
  String about;
  String address;
  String citizenId;
  double locationLat;
  double locationLng;

  Citizen({this.user, this.about, this.address, this.userId, this.fullName, this.phoneNumber, this.username});

  Citizen.fromMap(Map snapshot,String citizenId) :
        citizenId = citizenId ?? '',
        userId = snapshot['userId'],
        fullName = snapshot['fullName'] ?? '',
        username = snapshot['username'] ?? '',
        phoneNumber = snapshot['phoneNumber'] ?? '',
        about = snapshot['about'] ?? '',
        address = snapshot['address'] ?? '',
        locationLat = snapshot['locationLat'] ?? 0.0,
        locationLng = snapshot['locationLng'] ?? 0.0;

  toJson() {
    return {
      "userId": userId,
      "fullName": fullName,
      "username": username,
      "phoneNumber": phoneNumber,
      "about": about,
      "address": address,
      "locationLat": locationLat,
      "locationLng": locationLng,
    };
  }
}
