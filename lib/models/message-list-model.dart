class MessageList {
  List<String> messages;

  MessageList({this.messages});

  MessageList.fromMap(Map snapshot) :
    messages = snapshot ?? '';
}

