enum UserRoles {
  admin, 
  citizen,
  collector
}

const Map<UserRoles, String> getUserRole = {
  UserRoles.admin: "admin",
  UserRoles.citizen: "citizen",
  UserRoles.collector: "collector",
};