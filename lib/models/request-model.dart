

import 'package:heroji_reciklaze_2020/models/request-status.dart';

class Request {
  String requestId;
  RequestStatus status;
  String type;
  String description;
  String citizenId;
  String collectorId;
  String collectorName;
  String citizenName;
  String location;
  bool validatedByCitizen;
  bool validatedByCollector;
  DateTime createdAt;
  DateTime validatedAt;
  DateTime prefPickupTime;
  double locationLat;
  double locationLng;

  Request();

  Request.fromMap(Map snapshot,String requestId) :
        requestId = requestId ?? '',
        status = RequestStatus.values.firstWhere((e) => e.toString().split(".")[1] == snapshot['status']),
        type = snapshot['type'] ?? '',
        description = snapshot['description'] ?? '',
        citizenId = snapshot['citizenId'] ?? '',
        collectorId = snapshot['collectorId'] ?? '',
        collectorName = snapshot['collectorName'] ?? '',
        citizenName = snapshot['citizenName'] ?? '',
        location = snapshot['location'] ?? '',
        validatedByCitizen = snapshot['validatedByCitizen'] ?? false,
        validatedByCollector = snapshot['validatedByCollector'] ?? false,
        createdAt = snapshot['createdAt'] != null ? DateTime.fromMicrosecondsSinceEpoch(snapshot['createdAt'].microsecondsSinceEpoch) : null,
        prefPickupTime = snapshot['prefPickupTime'] != null ? DateTime.fromMicrosecondsSinceEpoch(snapshot['prefPickupTime'].microsecondsSinceEpoch) : null,
        validatedAt = snapshot['validatedAt'] != null ? DateTime.fromMicrosecondsSinceEpoch(snapshot['validatedAt'].microsecondsSinceEpoch) : null,
        locationLat = snapshot['locationLat'] ?? 0.0,
        locationLng = snapshot['locationLng'] ?? 0.0;

  toJson() {
    return {
      "requestId": requestId,
      "status": getRequestStatus[status],
      "type": type,
      "description": description,
      "citizenId": citizenId,
      "collectorName": collectorName,
      "citizenName": citizenName,
      "collectorId": collectorId,
      "location": location,
      "validatedByCitizen": validatedByCitizen,
      "validatedByCollector": validatedByCollector,
      "createdAt": createdAt,
      "validatedAt": validatedAt,
      "prefPickupTime": prefPickupTime,
      "locationLat": locationLat,
      "locationLng": locationLng,
    };
  }
}
