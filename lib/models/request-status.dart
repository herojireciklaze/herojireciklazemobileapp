enum RequestStatus {
  requested,
  accepted,
  fullfilled,
  denied
}

const Map<RequestStatus, String> getRequestStatus = {
  RequestStatus.requested: "requested",
  RequestStatus.accepted: "accepted",
  RequestStatus.denied: "denied",
  RequestStatus.fullfilled: "fullfilled"
};