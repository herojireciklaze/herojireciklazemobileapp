import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'package:heroji_reciklaze_2020/helpers/provider.dart';
import 'package:heroji_reciklaze_2020/helpers/auth.dart';
import 'package:heroji_reciklaze_2020/pages/admin-cms/collector-approval.dart';
import 'package:heroji_reciklaze_2020/pages/admin-cms/collector-list.dart';
import 'package:heroji_reciklaze_2020/pages/admin-cms/user-list.dart';
import 'package:heroji_reciklaze_2020/pages/citizen-cms/active-requests.dart';
import 'package:heroji_reciklaze_2020/pages/citizen-cms/citizen-landing-page.dart';
import 'package:heroji_reciklaze_2020/pages/citizen-cms/create-request.dart';
import 'package:heroji_reciklaze_2020/pages/citizen-cms/hero-status.dart';
import 'package:heroji_reciklaze_2020/pages/citizen-cms/my-profile-citizen.dart';
import 'package:heroji_reciklaze_2020/pages/collector-cms/active-requests-collector.dart';
import 'package:heroji_reciklaze_2020/pages/collector-cms/collector-landing-page.dart';
import 'package:heroji_reciklaze_2020/pages/collector-cms/map.dart';
import 'package:heroji_reciklaze_2020/pages/collector-cms/my-profile-collector.dart';
import 'package:heroji_reciklaze_2020/pages/home.dart';
import 'package:heroji_reciklaze_2020/pages/login.dart';
import 'package:heroji_reciklaze_2020/pages/dashboard.dart';
import 'package:heroji_reciklaze_2020/pages/splash-screen.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/widgets/chat.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Needed for IOS
    FirebaseMessaging firebaseMessaging = locator<FirebaseMessaging>();
    firebaseMessaging.requestNotificationPermissions();

    return Provider(
      auth: Auth(),
      child: MaterialApp(
          title: 'Flutter Demo',
          home: SplashScreen(),
          routes: <String, WidgetBuilder>{
            '/home': (BuildContext context) => HomePage(),
            '/login': (BuildContext context) => LoginPage(),
            '/dashboard': (BuildContext context) =>
                Dashboard(title: 'Pocetna stranica korisnika'),
            '/map': (BuildContext context) => MapPage(),
            '/my-profile-citizen': (BuildContext context) => MyProfileCitizen(),
            '/add-request': (BuildContext context) => CreateRequest(),
            '/collector-approval': (BuildContext context) =>
                CollectorApprovalPage(),
            '/hero-status': (BuildContext context) => HeroStatus(),
            '/citizen-list': (BuildContext context) => CitizenListPage(),
            '/collector-list': (BuildContext context) => CollectorListPage(),
            '/active-requests': (BuildContext context) => ActiveRequests(),
            '/collector-landing': (BuildContext context) =>
                CollectorLandingPage(),
            '/citizen-landing': (BuildContext context) => CitizenLandingPage(),
            '/active-requests-collectors': (BuildContext context) =>
                ActiveRequestsCollector(),
            '/my-profile-collector': (BuildContext context) =>
                MyProfileCollector(),
            '/chat': (BuildContext context) =>
                ChatScreen(),
          }),
    );
  }
}
