import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/helpers/constants.dart';

import '../helpers/auth.dart';
import '../helpers/provider.dart';

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Color backgroundColor = Colors.red;
  final Text title;
  final AppBar appBar;
  final List<Widget> widgets;
  final BuildContext originalContext;

  /// you can add more fields that meet your needs

  const BaseAppBar(
      {Key key, this.title, this.appBar, this.widgets, this.originalContext})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor:  Color(0xFF20806e),
      title: this.title,
      actions: <Widget>[
        PopupMenuButton<String>(
          onSelected: choiceAction,
          itemBuilder: (BuildContext context) {
            return Constants.choices.map((String choice) {
              return PopupMenuItem<String>(
                value: choice,
                child: Text(choice),
              );
            }).toList();
          },
        )
      ],
    );
  }

  Future<void> choiceAction(String choice) async {
    if (choice == Constants.SignOut) {
      print('SignOut');
      await signOut().then((val) {
        Navigator.popUntil(this.originalContext, (route) => route.isFirst);
        Navigator.pushReplacementNamed(this.originalContext, "/login");
      });
    }
  }

  Future<void> signOut() async {
    try {
      Auth auth = Provider.of(this.originalContext).auth;
      await auth.signOut();
    } catch (e) {
      print(e);
    }
  }

  @override
  Size get preferredSize {
    return new Size.fromHeight(this.appBar.preferredSize.height);
  }
}
