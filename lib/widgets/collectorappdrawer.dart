import 'package:flutter/material.dart';

class CollectorAppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
              height: MediaQuery.of(context).size.width * 0.35,
              child: DrawerHeader(
                child: Image.asset('assets/images/logoheader.png'),
                decoration: BoxDecoration(
                  color: Color(0xFF20806e),
                ),
              )),
          ListTile(
            title: Text('Moj profil'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
              Navigator.pushNamed(context, '/my-profile-collector');
            },
          ),
          ListTile(
            title: Text('Aktivni upiti'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
              Navigator.pushNamed(context, '/active-requests-collectors');
            },
          ),
          ListTile(
            title: Text('Svi upiti'),
            onTap: () {
              // Navigator.pop(context);
              // Navigator.pushNamed(context, '/collector-approval');
              // Update the state of the app
              // ...
              // Then close the drawer
              //Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Mapa aktivnih upita'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
              Navigator.pushNamed(context, '/map');
            },
          ),
        ],
      ),
    );
  }
}
