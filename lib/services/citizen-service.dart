import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:heroji_reciklaze_2020/models/citizen-model.dart';
import 'package:heroji_reciklaze_2020/models/user-model.dart';

import 'api.dart';
import 'locator.dart';

class CitizenService extends ChangeNotifier {
  Api _api = locator<Api>();
  String path = 'citizens';

  List<Citizen> citizens;


  Future<List<Citizen>> fetchCitizens() async {
    var result = await _api.getDataCollection(path);
    citizens = result.documents
        .map((doc) => Citizen.fromMap(doc.data, doc.documentID))
        .toList();
    return citizens;
  }

  Stream<QuerySnapshot> fetchCitizensAsStream() {
    return _api.streamDataCollection(path);
  }

  Future<Citizen> getCitizenById(String id) async {
    var doc = await _api.getDocumentById(path,id);
    return  Citizen.fromMap(doc.data, doc.documentID) ;
  }

  Future removeCitizen(String id) async{
     await _api.removeDocument(path,id) ;
     return ;
  }
  Future updateCitizens(Citizen data,String id) async{
    await _api.updateDocument(path,data.toJson(), id) ;
    return ;
  }

  Future addCitizen(Citizen data) async{
    var result  = await _api.addDocument(path,data.toJson()) ;

    return ;
  }

    Future<Citizen> getCitizenByUserId(String id) async {
    var ref = _api.getCollectionReference(path);
    QuerySnapshot result = await ref.where('userId', isEqualTo: id).limit(1).getDocuments();

    if(result.documents.length == 0) return null;

    DocumentSnapshot doc = result.documents.first;
    
    return Citizen.fromMap(doc.data, doc.documentID);
  }

}