import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:heroji_reciklaze_2020/models/request-model.dart';
import 'package:heroji_reciklaze_2020/models/request-status.dart';

import 'api.dart';
import 'locator.dart';

class RequestService extends ChangeNotifier {
  Api _api = locator<Api>();
  String path = 'requests';

  List<Request> requests;


  Future<List<Request>> fetchRequests() async {
    var result = await _api.getDataCollection(path);
    requests = result.documents
        .map((doc) => Request.fromMap(doc.data, doc.documentID)) 
        .toList();
    return requests;
  }

  Stream<QuerySnapshot> fetchRequestsForAsStream() {
    return _api.streamDataCollection(path);
  }

  Future<Request> getRequestById(String id) async {
    var doc = await _api.getDocumentById(path,id);
    return  Request.fromMap(doc.data, doc.documentID) ;
  }

  Future removeRequest(String id) async{
     await _api.removeDocument(path,id) ;
     return ;
  }
  Future updateRequest(Request data,String id) async{
    await _api.updateDocument(path,data.toJson(), id) ;
    return ;
  }

  Future addRequest(Request data) async{
    // TODO Notify collector
    var result  = await _api.addDocument(path,data.toJson()) ;

    return ;
  }

  Future validateRequestByCitizen(Request data) async{
    data.validatedByCitizen = true;
    if(data.validatedByCollector) data.status = RequestStatus.fullfilled;
    this.updateRequest(data, data.requestId);
    return;
  }

    Future validateRequestByCollector(Request data) async{
    data.validatedByCollector = true;
    if(data.validatedByCitizen) data.status = RequestStatus.fullfilled;
    this.updateRequest(data, data.requestId);
    return;
  }

  Future acceptRequestByCollector(Request data) async{
    data.status = RequestStatus.accepted;
    this.updateRequest(data, data.requestId);
    return;
  }

  Future denyRequestByCollector(Request data) async{
    data.status = RequestStatus.denied;
    this.updateRequest(data, data.requestId);
    return;
  }

  // Future getRequestsByCollectorId(String collectorId) async{
  //   var ref = await _api.getCollectionReference('requests');
  //   var result = ref.where('collectorId', isEqualTo: collectorId).snapshots();
  //   return result;
  // }

    Stream<QuerySnapshot> getRequestsByCollectorId(String citizenId) {
    var ref = _api.getCollectionReference('requests');
    var result = ref.where('collectorId', isEqualTo: citizenId).where('status', whereIn: ['requested','accepted']).snapshots();
    return result;
  }

  Stream<QuerySnapshot> getCitizenRequestsByStatus(String citizenId, RequestStatus status) {

    String statusString = status.toString().split(".")[1];

    var ref = _api.getCollectionReference('requests');
    var result = ref.where('citizenId', isEqualTo: citizenId).where('status', isEqualTo: statusString).snapshots();
    return result;
  }
}