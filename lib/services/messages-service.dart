import 'package:flutter/cupertino.dart';
import 'package:heroji_reciklaze_2020/models/message-model.dart';

import 'api.dart';
import 'locator.dart';

class MessagesService extends ChangeNotifier {
  Api _api = locator<Api>();
  String path = 'messages';

  List<Message> messages;

   Future fetchMessages(String fromUser, String toUser) async {
    var res = await _api.getCollectionReference(path);
    return res.where('toUser', isEqualTo: toUser).where('fromUser', isEqualTo: fromUser).snapshots();
    
  }
  Future sendMessage(Message data) async{
    var result  = await _api.addDocument(path,data.toJson()) ;
    return ;
  }
}
