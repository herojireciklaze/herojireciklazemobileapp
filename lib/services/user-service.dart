import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:heroji_reciklaze_2020/models/citizen-model.dart';
import 'package:heroji_reciklaze_2020/models/collector-model.dart';
import 'package:heroji_reciklaze_2020/models/user-model.dart';
import 'package:heroji_reciklaze_2020/models/user-roles.dart';

import 'api.dart';
import 'locator.dart';

class UserService extends ChangeNotifier {
  Api _api = locator<Api>();
  String path = 'users';

  List<User> users;


  Future<List<User>> fetchUsers() async {
    var result = await _api.getDataCollection(path);
    users = result.documents
        .map((doc) => User.fromMap(doc.data, doc.documentID))
        .toList();
    return users;
  }

  Stream<QuerySnapshot> fetchUsersAsStream() {
    return _api.streamDataCollection(path);
  }

  Future<User> getUserById(String id) async {
    var doc = await _api.getDocumentById(path,id);
    return  User.fromMap(doc.data, doc.documentID) ;
  }

  Future removeUser(String id) async{
     await _api.removeDocument(path,id) ;
     return ;
  }
  Future updateUsers(User data,String id) async{
    await _api.updateDocument(path,data.toJson(), id) ;
    return ;
  }

  Future addUserCitizen(User data, Citizen userData) async{
    _api.addDocument(path,data.toJson())
    .then((user) {
          userData.userId = user.documentID;
        _api.addDocument('citizens', userData.toJson());
    }) ;

    return ;
  }

    Future addUserCollector(User data, Collector userData) async{
    _api.addDocument(path,data.toJson())
     .then((user) {
          userData.userId = user.documentID;
        _api.addDocument('collectors', userData.toJson());
    }) ;
 

    return ;
  }

  Future<User> getUserByFbId(String id) async {
    var ref = _api.getCollectionReference(path);
    QuerySnapshot result = await ref.where('fbUid', isEqualTo: id).limit(1).getDocuments();

    if(result.documents.length == 0) return null;

    DocumentSnapshot doc = result.documents.first;
    
    return User.fromMap(doc.data, doc.documentID);
  }

}