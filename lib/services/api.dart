import 'package:cloud_firestore/cloud_firestore.dart';

class Api{
  final Firestore _db = Firestore.instance;
  //String path;
  CollectionReference ref;

  Api() {
   // ref = _db.collection(path);
  }

  getRef(String path) {
    return _db.collection(path);
  }

  Future<QuerySnapshot> getDataCollection(String path) {
    return getRef(path).getDocuments() ;
  }
  CollectionReference getCollectionReference(String path){
    return getRef(path);
  }
  Stream<QuerySnapshot> streamDataCollection(String path) {
    return  getRef(path).snapshots() ;
  }
  Future<DocumentSnapshot> getDocumentById(String path, String id) {
    return  getRef(path).document(id).get();
  }
  Future<void> removeDocument(String path,String id){
    return  getRef(path).document(id).delete();
  }
  Future<DocumentReference> addDocument(String path, Map data) {
    return  getRef(path).add(data);
  }
  Future<void> updateDocument(String path,Map data , String id) {
    return  getRef(path).document(id).updateData(data) ;
  }


}