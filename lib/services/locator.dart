import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:heroji_reciklaze_2020/services/citizen-service.dart';
import 'package:heroji_reciklaze_2020/services/collector-service.dart';
import 'package:heroji_reciklaze_2020/services/messages-service.dart';
import 'package:heroji_reciklaze_2020/services/request-service.dart';
import 'package:heroji_reciklaze_2020/services/user-service.dart';

import 'api.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => CitizenService());
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => CollectorService());
  locator.registerLazySingleton(() => RequestService());
  locator.registerLazySingleton(() => FirebaseMessaging());
  locator.registerLazySingleton(() => MessagesService());
}