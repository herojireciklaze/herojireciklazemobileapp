import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:heroji_reciklaze_2020/models/collector-model.dart';

import 'api.dart';
import 'locator.dart';

class CollectorService extends ChangeNotifier {
  Api _api = locator<Api>();
  String path = 'collectors';

  List<Collector> collectors;

  Future<List<Collector>> fetchCollectors() async {
    var result = await _api.getDataCollection(path);
    collectors = result.documents
        .map((doc) => Collector.fromMap(doc.data, doc.documentID))
        .toList();
    return collectors;
  }

  Stream<QuerySnapshot> fetchCollectorsAsStream() {
    return _api.streamDataCollection(path);
  }

  Future<Collector> getCollectorById(String id) async {
    var doc = await _api.getDocumentById(path, id);
    return Collector.fromMap(doc.data, doc.documentID);
  }

  Future<List<Collector>> getPendingCollectors() async {
    var ref =  _api.getCollectionReference(path);
    var result  = await ref.where('approvedByAdmin', isNull: true).getDocuments();
    var collectorsList = result.documents
            .map((doc) => Collector.fromMap(doc.data, doc.documentID)).toList();

    return collectorsList;
  }

      Future<Collector> getCollectorByUserId(String id) async {
    var ref = _api.getCollectionReference(path);
    QuerySnapshot result = await ref.where('userId', isEqualTo: id).limit(1).getDocuments();

    if(result.documents.length == 0) return null;

    DocumentSnapshot doc = result.documents.first;
    
    return Collector.fromMap(doc.data, doc.documentID);
  }

  Future removeCollector(String id) async {
    await _api.removeDocument(path, id);
    return;
  }


  Future updateCollectors(Collector data, String id) async {
    await _api.updateDocument(path, data.toJson(), id);
    return;
  }

  Future addCollector(Collector data) async {
    var result = await _api.addDocument(path, data.toJson());

    return;
  }
}
