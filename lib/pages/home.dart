import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/helpers/auth.dart';
import 'package:heroji_reciklaze_2020/helpers/provider.dart';
import 'package:heroji_reciklaze_2020/models/user-roles.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/services/user-service.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
class _HomePageState extends State<HomePage> {
  final formKey = GlobalKey<FormState>();

@override
  Widget build(BuildContext context) {
      final Auth auth = Provider.of(context).auth;
      UserService userService = locator<UserService>();
      auth.currentUser().then((currentUser) => {
              if(currentUser != null){

              userService.getUserByFbId(currentUser).then((user) => {
                
                if(user.role == UserRoles.citizen){
                  Navigator.pushReplacementNamed(context, '/citizen-landing')
                }
                else if(user.role == UserRoles.collector){
                  Navigator.pushReplacementNamed(context, '/collector-landing')
                }
                else if(user.role == UserRoles.admin){
                  Navigator.pushReplacementNamed(context, '/collector-approval')
                }
              })
            }
            else {
         Navigator.pushReplacementNamed(context, '/login')
      }
      });

     return Container();

  }

}

  
