import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';

class HeroStatus extends StatefulWidget {
  @override
  _HeroStatus createState() => _HeroStatus();
}

class _HeroStatus extends State<HeroStatus> {
  final String _pet = "10";
  final String _glass = "5";
  final String _can = "7";
  final String _paper = "9";

Widget _buildImage(Size screenSize) {
    return Center(
      child: Container(
        width: screenSize.width,
        height: screenSize.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/statusheroja.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: BaseAppBar(
            title: Text('Status heroja'), originalContext: context, appBar: AppBar()),
        body: _buildImage(screenSize));
  }
}
