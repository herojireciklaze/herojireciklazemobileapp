import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/helpers/auth.dart';
import 'package:heroji_reciklaze_2020/helpers/provider.dart';
import 'package:heroji_reciklaze_2020/models/request-model.dart';
import 'package:heroji_reciklaze_2020/models/request-status.dart';
import 'package:heroji_reciklaze_2020/models/user-model.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/services/request-service.dart';
import 'package:heroji_reciklaze_2020/services/user-service.dart';

import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/loader.dart';
import 'package:jiffy/jiffy.dart';

class ActiveRequests extends StatefulWidget {
  ActiveRequests({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ActiveRequestsState createState() => _ActiveRequestsState();
}

Image buildIcon(String type) {
  if (type == "Staklo")
    return Image.asset('assets/images/staklokanta.png');
  else if (type == "Karton")
    return Image.asset('assets/images/papirkanta.png');
  else if (type == "Limenke")
    return Image.asset('assets/images/limenkekanta.png');
  else
    return Image.asset('assets/images/plastikakanta.png');
}

// Icon buildStatus(RequestStatus status) {
//   if (status == RequestStatus.requested || status == RequestStatus.denied)
//     return Icon(Icons.cancel, color: Colors.red);
//   else
//     return Icon(Icons.check, color: Colors.green);
// }

Icon buildAction(RequestStatus status) {
  if (status == RequestStatus.requested || status == RequestStatus.denied)
    return Icon(Icons.cancel, color: Colors.red);
  else
    return Icon(Icons.check, color: Colors.green);
}

void doAction(RequestStatus status) {
// if (status == RequestStatus.requested || status == RequestStatus.denied)
// user validated request
// else delete request
}

class _ActiveRequestsState extends State<ActiveRequests> {
  //@override
  //Widget build(BuildContext context) {
  //return Scaffold(
  //    backgroundColor: Colors.green,
  //  appBar: BaseAppBar(
  //    title: Text('Aktivni upiti'),
  //  originalContext: context,
  //appBar: AppBar()),
  //body: makeBody);
  //}
  @override
  void initState() {
    super.initState();
  }

  Future<String> getMyCitizenId() async {
    Auth auth = Provider.of(context).auth;
    UserService userService = locator<UserService>();

    String fbId = await auth.currentUser();
    User user = await userService.getUserByFbId(fbId);

    return user.userId;
  }

  ListTile makeListTile(Request request) => ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(width: 1.0, color: Colors.white24))),
            //child: Icon(Icons.autorenew, color: Colors.white),
            child: buildIcon(request.type)),
        title: Text(
          "Sakupljač: " + request.collectorName,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

        subtitle: Row(
          children: <Widget>[
            Expanded(
              flex: 4,
              child: Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Text("Datum: " + Jiffy(request.prefPickupTime).yMd,
                      style: TextStyle(color: Colors.white))),
            ),
          ],
        ),
        trailing:
            //Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0),
            buildAction(request.status),
        onTap: () {
          doAction(request.status);
        },
      );

  Card makeCard(Request request) => Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child: makeListTile(request),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getMyCitizenId(),
        builder: (context, snap) {
          if (snap.connectionState != ConnectionState.done ||
              snap.hasData == null) {
            return Container();
          }

          String myUserId = snap.data;
          RequestService requestService = locator<RequestService>();

          return StreamBuilder(
              stream: requestService.getCitizenRequestsByStatus(
                  myUserId, RequestStatus.requested),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  var requests = snapshot.data.documents
                      .map((doc) => Request.fromMap(doc.data, doc.documentID))
                      .toList();

                  final makeBody = Container(
                    // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
                    child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: requests.length,
                      itemBuilder: (BuildContext context, int index) {
                        return makeCard(requests[index]);
                      },
                    ),
                  );

                  return Scaffold(
                    backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
                    appBar: BaseAppBar(
                        title: Text('Active requests'),
                        originalContext: context,
                        appBar: AppBar()),
                    body: makeBody,
                  );
                } else {
                  return CustomLoader();
                }
              });
        });
  }
}
