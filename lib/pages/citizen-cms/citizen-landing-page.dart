import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/appdrawer.dart';

class CitizenLandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          originalContext: context,
        ),
        drawer: UserAppDrawer(),
        body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/progres.jpg'),
            fit: BoxFit.cover
        ) ,
      )),
        );
  }
}
