import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/helpers/auth.dart';
import 'package:heroji_reciklaze_2020/helpers/provider.dart';
import 'package:heroji_reciklaze_2020/models/citizen-model.dart';
import 'package:heroji_reciklaze_2020/models/collector-model.dart';
import 'package:heroji_reciklaze_2020/models/request-model.dart';
import 'package:heroji_reciklaze_2020/models/request-status.dart';
import 'package:heroji_reciklaze_2020/models/user-model.dart';
import 'package:heroji_reciklaze_2020/services/citizen-service.dart';
import 'package:heroji_reciklaze_2020/services/collector-service.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/services/request-service.dart';
import 'package:heroji_reciklaze_2020/services/user-service.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/dialog.dart';
import 'package:jiffy/jiffy.dart';

class CreateRequest extends StatefulWidget {
  @override
  _CreateRequestState createState() => _CreateRequestState();
}

enum Categorie { PET, Karton, Limenke, Staklo }

class _CreateRequestState extends State<CreateRequest> {
  DateTime selectedDate = DateTime.now().add(Duration(days: 1));
  List<Collector> _collectors = [];
  Collector _selectedCollector;
  bool _selected;
  TextStyle _nameTextStyle = TextStyle(
    fontFamily: 'Spectral',
    color: Colors.white,
    fontSize: 28.0,
    fontWeight: FontWeight.w700,
  );

  @override
  void initState() {
    super.initState();

    _selected = false;
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: selectedDate,
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked; // October 18, 2019
      });
  }

  //final String _adresa = "Bulevar Oslobodjenja";
  Categorie _categorie;

  Widget _buildCategories(Size screenSize) {
    return Column(
      children: <Container>[
        Container(
          child: Center(
              child: Text(
            "IZABERITE KATEGORIJU",
            style: _nameTextStyle,
          )),
          height: screenSize.height * 0.2,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/cover.jpg'),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: <Widget>[
                  Column(
                    children: [
                      Image(
                        height: screenSize.width * 0.4,
                        width: screenSize.width * 0.4,
                        image: AssetImage('assets/images/plastika.png'),
                      ),
                      FlatButton(
                        color: Colors.teal,
                        child: Text(
                          "PLASTIKA",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            _selected = true;
                            _categorie = Categorie.PET;
                          });
                        },
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Image(
                        height: screenSize.width * 0.4,
                        width: screenSize.width * 0.4,
                        image: AssetImage('assets/images/staklo.png'),
                      ),
                      FlatButton(
                        color: Colors.teal,
                        child: Text(
                          "STAKLO",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            _selected = true;
                            _categorie = Categorie.Staklo;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Column(
                    children: [
                      Image(
                        height: screenSize.width * 0.4,
                        width: screenSize.width * 0.4,
                        image: AssetImage('assets/images/papir.png'),
                      ),
                      FlatButton(
                        color: Colors.teal,
                        child: Text(
                          "KARTON",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            _selected = true;
                            _categorie = Categorie.Karton;
                          });
                        },
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Image(
                        height: screenSize.width * 0.4,
                        width: screenSize.width * 0.4,
                        image: AssetImage('assets/images/limenke.png'),
                      ),
                      FlatButton(
                        color: Colors.teal,
                        child: Text(
                          "LIMENKE",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            _selected = true;
                            _categorie = Categorie.Limenke;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _buildCoverImage(Size screenSize) {
    return Container(
      height: screenSize.height * 0.2,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/cover.jpg'),
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget _buildProfileImage(Size screenSize) {
    return Center(
      child: Container(
        width: screenSize.width * 0.40,
        height: screenSize.width * 0.40,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/logo.jpg'),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(80.0),
          border: Border.all(
            color: Colors.white,
            width: 8.0,
          ),
        ),
      ),
    );
  }

  Widget _sendRequest(Size screenSize, Categorie _chosenCategorie) {
    CollectorService collectorService = locator<CollectorService>();

    return FutureBuilder(
        future: collectorService.fetchCollectors(),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done ||
              snapshot.hasData == null) {
            return Container();
          }
          // TODO: make query filter by trash type
          _collectors = snapshot.data;
          _selectedCollector = _collectors.first;

          return Container(
              width: screenSize.width,
              height: screenSize.height,
              //margin: new EdgeInsets.symmetric(horizontal: screenSize.width * 0.2, vertical: screenSize.height * 0.1),
              child: Column(
                children: <Widget>[
                  Container(
                      margin: new EdgeInsets.only(
                          right: screenSize.width * 0.2,
                          left: screenSize.width * 0.2,
                          top: screenSize.height * 0.2),
                      child: DropdownButton<Collector>(
                        value: _collectors.first,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        style: TextStyle(color: Colors.deepPurple),
                        underline: Container(
                          height: 2,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (Collector newValue) {
                          setState(() {
                            _selectedCollector = newValue;
                          });
                        },
                        items: _collectors.map<DropdownMenuItem<Collector>>(
                            (Collector value) {
                          return DropdownMenuItem<Collector>(
                            value: value,
                            child: Text(value.fullName),
                          );
                        }).toList(),
                      )),
                  RaisedButton(
                    onPressed: () =>
                        {_selectDate(context), print(selectedDate.toString())},
                    child: Text(Jiffy(selectedDate).yMMMMd // October 18, 2019
                        ),
                  ),
                  RaisedButton(
                    child: Text('Dodaj upit'),
                    color: Colors.blueAccent,
                    onPressed: submit,
                  ),
                ],
              ));
        });
  }

  Future<String> getMyUserId() async {
    Auth auth = Provider.of(context).auth;
    UserService userService = locator<UserService>();

    String fbId = await auth.currentUser();
    User user = await userService.getUserByFbId(fbId);

    return user.userId;
  }


  Future<Citizen> getMyCitizen() async {
    Auth auth = Provider.of(context).auth;
    UserService userService = locator<UserService>();
    CitizenService citService = locator<CitizenService>();

    String fbId = await auth.currentUser();
    User user = await userService.getUserByFbId(fbId);
    Citizen cit = await citService.getCitizenByUserId(user.userId);

    return cit;
  }

  void submit() async {
    //should send a request to the database! and after that show dialog

    RequestService requestService = locator<RequestService>();

    String myId = await getMyUserId();
    Citizen citizen = await getMyCitizen();

    Request request = Request();
    request.citizenId = myId;
    request.collectorId = _selectedCollector.userId;
    request.collectorName = _selectedCollector.fullName;
    request.citizenName = citizen.fullName;
    request.createdAt = DateTime.now();
    request.status = RequestStatus.requested;
    request.type = _categorie.toString().split(".")[1];
    request.prefPickupTime = selectedDate;
    request.validatedByCitizen = false;
    request.validatedByCollector = false;
    request.locationLng = citizen.locationLng;
    request.locationLat = citizen.locationLat;

    requestService.addRequest(request).then((value) {
      showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
                title: "Pravi si heroj!",
                description: "Hvala što pomažeš zaštiti životne sredine!",
                buttonText: "U redu",
              ));
    });
  }

  @override
  Widget build(BuildContext context) {
    print(_selected);
    Size screenSize = MediaQuery.of(context).size;
    if (!_selected)
      return Scaffold(
        appBar: BaseAppBar(originalContext: context, appBar: AppBar()),
        body: Stack(
          children: <Widget>[_buildCategories(screenSize)],
        ),
      );
    else
      return Scaffold(
          appBar: BaseAppBar(originalContext: context, appBar: AppBar()),
          body: Stack(children: <Widget>[
            _buildCoverImage(screenSize),
            SafeArea(
                child: SingleChildScrollView(
                    child: Column(
              children: <Widget>[
                _buildProfileImage(screenSize),
                _sendRequest(screenSize, _categorie)
              ],
            ))),
          ]));
  }
}
