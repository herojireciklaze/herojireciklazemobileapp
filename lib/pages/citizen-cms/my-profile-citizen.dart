import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/models/citizen-model.dart';
import 'package:heroji_reciklaze_2020/models/user-model.dart';
import 'package:heroji_reciklaze_2020/services/citizen-service.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/services/user-service.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/loader.dart';

import '../../helpers/auth.dart';
import '../../helpers/provider.dart';

class MyProfileCitizen extends StatefulWidget {
  @override
  _MyProfileCitizenState createState() => _MyProfileCitizenState();
}

class _MyProfileCitizenState extends State<MyProfileCitizen> {
  final String _pet = "10";
  final String _glass = "5";
  final String _can = "7";
  final String _paper = "9";

  Widget _buildCoverImage(Size screenSize) {
    return Container(
      height: screenSize.height * 0.2,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/cover.jpg'),
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget _buildProfileImage(Size screenSize) {
    return Center(
      child: Container(
        width: screenSize.width * 0.40,
        height: screenSize.width * 0.40,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/profil.png'),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(80.0),
          border: Border.all(
            color: Colors.white,
            width: 8.0,
          ),
        ),
      ),
    );
  }

  Widget _buildFullName(Citizen citizen) {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      citizen.fullName,
      style: _nameTextStyle,
    );
  }

  Widget _buildPhoneNumber(BuildContext context, Citizen citizen) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 6.0),
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.phone_android, color: Colors.black),
            Text(
              citizen.phoneNumber,
              
              style: TextStyle(
                fontFamily: 'Spectral',
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.w300,
              ),
            ),
          ],
        ));
  }

  Widget _buildAddress(BuildContext context, Citizen citizen) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 6.0),
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.location_on, color: Colors.black),
            Text(
              citizen.address,
              style: TextStyle(
                fontFamily: 'Spectral',
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.w300,
              ),
            ),
          ],
        ));
  }

  Widget _buildUsername(BuildContext context, Citizen citizen) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Text(
        citizen.username,
        style: TextStyle(
          fontFamily: 'Spectral',
          color: Colors.black,
          fontSize: 20.0,
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }

  Widget _buildStatItem(String label, String count) {
    TextStyle _statLabelTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 16.0,
      fontWeight: FontWeight.w200,
    );

    TextStyle _statCountTextStyle = TextStyle(
      color: Colors.black54,
      fontSize: 24.0,
      fontWeight: FontWeight.bold,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          count,
          style: _statCountTextStyle,
        ),
        Text(
          label,
          style: _statLabelTextStyle,
        ),
      ],
    );
  }

  Widget _buildStatContainer() {
    return Container(
      height: 60.0,
      margin: EdgeInsets.only(top: 8.0),
      decoration: BoxDecoration(
        color: Color(0xFFEFF4F7),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildStatItem("PET", _pet),
          _buildStatItem("STAKLO", _glass),
          _buildStatItem("LIMENKE", _can),
          _buildStatItem("KARTON", _paper),
        ],
      ),
    );
  }

  Widget _buildBio(BuildContext context, Citizen citizen) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'Spectral',
      fontWeight: FontWeight.w400,
      //try changing weight to w500 if not thin
      fontStyle: FontStyle.italic,
      color: Color(0xFF799497),
      fontSize: 16.0,
    );

    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.all(8.0),
      child: Text(
        citizen.about,
        textAlign: TextAlign.center,
        style: bioTextStyle,
      ),
    );
  }

  Widget _buildSeparator(Size screenSize) {
    return Container(
      width: screenSize.width / 1.6,
      height: 2.0,
      color: Colors.black54,
      margin: EdgeInsets.only(top: 4.0),
    );
  }

  Widget _buildButtons(Size screenSize) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: 20.0, horizontal: screenSize.width * 0.2),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () => Navigator.pushNamed(context, '/add-request'),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
                  border: Border.all(),
                  color: Color(0xFF404A5C),
                ),
                child: Center(
                  child: Text(
                    "DODAJ UPIT",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<Citizen> getCurrentCitizen() async {
    final Auth auth = Provider.of(context).auth;
    CitizenService citizenService = locator<CitizenService>();
    UserService userService = locator<UserService>();

    String fbId = await auth.currentUser();
    User user = await userService.getUserByFbId(fbId);
    return citizenService.getCitizenByUserId(user.userId);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getCurrentCitizen(),
        builder: (context, citizenSnapshot) {
          if (citizenSnapshot.connectionState != ConnectionState.done ||
              citizenSnapshot.hasData == null) {
            return CustomLoader();
          }

          Citizen citizen = citizenSnapshot.data;

          Size screenSize = MediaQuery.of(context).size;
          return Scaffold(
            appBar: BaseAppBar(title: Text("Moj profil"),originalContext: context,appBar: AppBar()),
            body: Stack(
              children: <Widget>[
                _buildCoverImage(screenSize),
                SafeArea(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: screenSize.height * 0.1),
                        _buildProfileImage(screenSize),
                        _buildFullName(citizen),
                        _buildUsername(context, citizen),
                        SizedBox(height: 8.0),
                        _buildStatContainer(),
                        SizedBox(height: 8.0),
                        _buildBio(context, citizen),
                        _buildSeparator(screenSize),
                        _buildPhoneNumber(context, citizen),
                        _buildAddress(context, citizen),
                        _buildButtons(screenSize),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
