import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:heroji_reciklaze_2020/helpers/provider.dart';
import 'package:heroji_reciklaze_2020/helpers/validators.dart';
import 'package:heroji_reciklaze_2020/models/citizen-model.dart';
import 'package:heroji_reciklaze_2020/models/collector-model.dart';
import 'package:heroji_reciklaze_2020/models/user-model.dart';
import 'package:heroji_reciklaze_2020/models/user-roles.dart';
import 'package:heroji_reciklaze_2020/services/collector-service.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/services/user-service.dart';
import 'package:heroji_reciklaze_2020/widgets/dialog.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = GlobalKey<FormState>();
  UserService userService = locator<UserService>();
  CollectorService collectorService = locator<CollectorService>();

  String _korisnici = "Korisnik";
  List<String> categories = [];
  String _email, _password, _username, _fullName, _phoneNumber, _address;
  FormType _formType = FormType.login;
  bool disable = false;

  bool validate() {
    final form = formKey.currentState;
    form.save();
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void onLogin(String userId, String method) {
    print('Signed in $userId with $method');

    User user = new User();
    FirebaseMessaging firebaseMessaging = locator<FirebaseMessaging>();
    firebaseMessaging.getToken().then((token) {
      print('token: $token');

      userService.getUserByFbId(userId).then((userObj) {
        user = userObj;
        if (user != null) {
          user.pushToken = token;
          userService.updateUsers(user, user.userId);
          if (user.role == UserRoles.citizen) {
            Navigator.pushReplacementNamed(context, "/citizen-landing");
          } else if (user.role == UserRoles.collector) {
            collectorService
                .getCollectorByUserId(user.userId)
                .then((collector) => {
                      if (collector.approvedByAdmin == false)
                        {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) => CustomDialog(
                              title: "Obaveštenje",
                              description:
                                  "Vaša registracija još uvek nije odobrena.",
                              buttonText: "U redu",
                            ),
                          )
                        }
                      else
                        {
                          Navigator.pushReplacementNamed(
                              context, "/collector-landing")
                        }
                    });
          } else if (user.role == UserRoles.admin) {
            Navigator.pushReplacementNamed(context, "/collector-approval");
          }
        }
      });
    });
  }

  void submit() async {
    if (validate()) {
      try {
        final auth = Provider.of(context).auth;
        // LOGIN
        if (_formType == FormType.login) {
          String userId = await auth.signInWithEmailAndPassword(
            _email,
            _password,
          );

          onLogin(userId, "Email");
        }
        // REGISTER
        else {
          String userId = await auth.createUserWithEmailAndPassword(
            _email,
            _password,
          );

          FirebaseMessaging firebaseMessaging = locator<FirebaseMessaging>();
          String token = await firebaseMessaging.getToken();

          User user = User();
          user.fbUid = userId;
          user.pushToken = token;

          if (_korisnici == "Korisnik") {
            user.role = UserRoles.citizen;
            Citizen citizen = new Citizen();
            citizen.username = _username;
            citizen.fullName = _fullName;
            citizen.address = _address;
            citizen.user = user;
            citizen.phoneNumber = _phoneNumber;

            final query = _address;
            var addresses = await Geocoder.local.findAddressesFromQuery(query);

            if(addresses.length > 0) {
              var first = addresses.first;
              citizen.locationLat = first.coordinates.latitude;
              citizen.locationLng = first.coordinates.longitude;
            }

            userService.addUserCitizen(user, citizen);
            showDialog(
              context: context,
              builder: (BuildContext context) => CustomDialog(
                title: "Obaveštenje",
                description: "Vaša registracija je uspešna!",
                buttonText: "U redu",
              ),
            );
            switchFormState('login');
          } else if (_korisnici == "Sakupljač") {
            user.role = UserRoles.collector;
            Collector collector = new Collector();
            collector.username = _username;
            collector.user = user;
            collector.fullName = _fullName;
            collector.address = _address;
            collector.phoneNumber = _phoneNumber;
            collector.type = categories;
            userService.addUserCollector(user, collector);
            showDialog(
              context: context,
              builder: (BuildContext context) => CustomDialog(
                title: "Obaveštenje",
                description:
                    "Bićete obavešteni kada Vam administrator odobri registraciju.",
                buttonText: "U redu",
              ),
            );
          }

          print('Registered in $userId');
        }
      } catch (e) {
        print(e);
      }
    }
  }

  void switchFormState(String state) {
    formKey.currentState.reset();

    if (state == 'register') {
      setState(() {
        _formType = FormType.register;
      });
    } else {
      setState(() {
        _formType = FormType.login;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    categories = [];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF20806e),
        title: Text('Heroji reciklaže 2020'),
      ),
      resizeToAvoidBottomPadding: false,
      body: Center(
          child: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ...buildInputs(screenSize),
              ...buildButtons(screenSize),
            ],
          ),
        ),
      )),
    );
  }

  List<Widget> buildSameInputs() {
    return [
      TextFormField(
        validator: EmailValidator.validate,
        decoration: InputDecoration(labelText: 'Email'),
        onSaved: (value) => _email = value,
      ),
      TextFormField(
        validator: PasswordValidator.validate,
        decoration: InputDecoration(labelText: 'Password'),
        obscureText: true,
        onSaved: (value) => _password = value,
      ),
    ];
  }

  List<Widget> buildInputs(Size screenSize) {
    // LOGIN
    if (_formType == FormType.login) {
      return [
        Container(
            margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.15),
            child: TextFormField(
              validator: EmailValidator.validate,
              decoration: InputDecoration(labelText: 'Email'),
              onSaved: (value) => _email = value,
            )),
        Container(
            margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.15),
            child: TextFormField(
              validator: PasswordValidator.validate,
              decoration: InputDecoration(labelText: 'Šifra'),
              obscureText: true,
              onSaved: (value) => _password = value,
            )),
      ];
    } else {
      // REGISTER
      return [
        Container(
          margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.2),
          child: RadioButtonGroup(
            orientation: GroupedButtonsOrientation.HORIZONTAL,
            onSelected: (String selected) => setState(() {
              _korisnici = selected;
            }),
            labels: <String>["Korisnik", "Sakupljač"],
            picked: _korisnici,
            onChange: (_korisnik, _1) =>
                {checkDisable(_korisnik, categories.isEmpty)},
            itemBuilder: (Radio rb, Text txt, int i) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[rb, txt],
              );
            },
          ),
        ),
        Container(
            margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.15),
            child: TextFormField(
              validator: EmailValidator.validate,
              decoration: InputDecoration(labelText: 'Email'),
              onSaved: (value) => _email = value.trim(),
            )),
        Container(
            margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.15),
            child: TextFormField(
              validator: RequiredFieldsValidator.validate,
              decoration: InputDecoration(labelText: 'Ime'),
              onSaved: (value) => _fullName = value,
            )),
        Container(
            margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.15),
            child: TextFormField(
              validator: RequiredFieldsValidator.validate,
              decoration: InputDecoration(labelText: 'Korisnicko ime'),
              onSaved: (value) => _username = value,
            )),
        Container(
            margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.15),
            child: TextFormField(
              validator: PhoneNumberValidator.validate,
              decoration: InputDecoration(labelText: 'Broj telefona'),
              onSaved: (value) => _phoneNumber = value,
            )),
        Container(
            margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.15),
            child: TextFormField(
              validator: RequiredFieldsValidator.validate,
              decoration: InputDecoration(labelText: 'Adresa'),
              onSaved: (value) => _address = value,
            )),
        Container(
            margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.15),
            child: TextFormField(
              validator: PasswordValidator.validate,
              decoration: InputDecoration(labelText: 'Šifra'),
              obscureText: true,
              onSaved: (value) => _password = value,
            )),
        if (_korisnici == 'Sakupljač')
          Container(
              margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.22),
              child: CheckboxGroup(
                  orientation: GroupedButtonsOrientation.HORIZONTAL,
                  labels: <String>["PET", "Staklo", "Karton", "Limenke"],
                  onSelected: (List<String> checked) => {
                        categories = checked,
                        checkDisable(_korisnici, categories.isEmpty),
                      })),
      ];
    }
  }

  void checkDisable(String korisnik, bool isEmpty) {
    setState(() {
      if (korisnik == 'Korisnik')
        disable = false;
      else {
        if (isEmpty)
          disable = true;
        else
          disable = false;
      }
    });
  }

  List<Widget> buildButtons(Size screenSize) {
    if (_formType == FormType.login) {
      return [
        Container(
          margin: new EdgeInsets.only(
              left: screenSize.width * 0.2,
              right: screenSize.width * 0.2,
              top: 50.0),
          // width: screenSize.width * 0.6,
          child: RaisedButton(
            child: Text('Uloguj se'),
            color: Colors.blueAccent,
            onPressed: submit,
          ),
        ),
        Container(
            margin:
                new EdgeInsets.symmetric(horizontal: screenSize.width * 0.2),
            child: FlatButton(
              child: Text('Registruj se'),
              color: Colors.teal,
              onPressed: () {
                switchFormState('register');
              },
            )),
        // FlatButton(
        //   child: Text("Sign in with Google"),
        //   color: Colors.lightGreen,
        //   onPressed: () async {
        //     try {
        //       final _auth = Provider.of(context).auth;
        //       final id = await _auth.signInWithGoogle();

        //       onLogin(id, "Google");
        //     } catch (e) {
        //       print(e);
        //     }
        //   },
        // ),
      ];
    } else {
      return [
        Container(
            margin: new EdgeInsets.only(
                left: screenSize.width * 0.2,
                right: screenSize.width * 0.2,
                top: 50.0),
            child: RaisedButton(
              child: Text('Registruj se'),
              color: Colors.blueAccent,
              onPressed: disable ? null : submit,
            )),
        Container(
            margin:
                new EdgeInsets.symmetric(horizontal: screenSize.width * 0.2),
            child: FlatButton(
              child: Text('Uloguj se'),
              color: Colors.teal,
              onPressed: () {
                switchFormState('login');
              },
            ))
      ];
    }
  }
}
