import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: <Widget>[
          RaisedButton(
            child: Text('Idi na login'),
            onPressed: () {},
          ),
          RaisedButton(
            child: Text('Idi na register'),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
