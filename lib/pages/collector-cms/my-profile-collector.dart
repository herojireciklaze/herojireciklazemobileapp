import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/models/collector-model.dart';
import 'package:heroji_reciklaze_2020/models/user-model.dart';
import 'package:heroji_reciklaze_2020/services/collector-service.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/services/user-service.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/loader.dart';

import '../../helpers/auth.dart';
import '../../helpers/provider.dart';

class MyProfileCollector extends StatefulWidget {
  @override
  _MyProfileCollectorState createState() => _MyProfileCollectorState();
}

class _MyProfileCollectorState extends State<MyProfileCollector> {
  final String _pet = "10";
  final String _glass = "5";
  final String _can = "7";
  final String _paper = "9";

  Widget _buildCoverImage(Size screenSize) {
    return Container(
      height: screenSize.height * 0.2,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/cover.jpg'),
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget _buildProfileImage(Size screenSize) {
    return Center(
      child: Container(
        width: screenSize.width * 0.40,
        height: screenSize.width * 0.40,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/profil.png'),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(80.0),
          border: Border.all(
            color: Colors.white,
            width: 8.0,
          ),
        ),
      ),
    );
  }

  Widget _buildFullName(Collector collector) {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      collector.fullName,
      style: _nameTextStyle,
    );
  }

  Widget _buildPhoneNumber(BuildContext context, Collector collector) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 6.0),
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.phone_android, color: Colors.black),
            Text(
              collector.phoneNumber,
              style: TextStyle(
                fontFamily: 'Spectral',
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.w300,
              ),
            ),
          ],
        ));
  }

  Widget _buildAddress(BuildContext context, Collector collector) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 6.0),
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.location_on, color: Colors.black),
            Text(
              collector.address,
              style: TextStyle(
                fontFamily: 'Spectral',
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.w300,
              ),
            ),
          ],
        ));
  }

  Widget _buildUsername(BuildContext context, Collector collector) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Text(
        collector.username,
        style: TextStyle(
          fontFamily: 'Spectral',
          color: Colors.black,
          fontSize: 20.0,
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }

  Widget _buildStatItem(String star) {
    return Image.asset("assets/images/leaf_$star.png");
  }

  List<String> ratingToStars(double rating) {

    if(rating < 0) rating = 0;
    else if(rating > 5) rating = 5;

    List<String> list = List();

    int fullStars = rating.floor();
    int halfStars = 0;

    double partialRating = rating - fullStars; // 0 - 1

    if(partialRating < 0.25) halfStars = 0;
    else if(partialRating < 0.75) halfStars = 1;
    else {
      halfStars = 0;
      fullStars--;
    }

    int emptyStars = 5 - (fullStars + halfStars);

    for(int i = 0; i < fullStars; i++) list.add("full");
    for(int i = 0; i < halfStars; i++) list.add("half");
    for(int i = 0; i < emptyStars; i++) list.add("empty");

    return list;
  }

  Widget _buildStatContainer() {

    double rating = 3.5;

    List<Widget> stars = List();

    ratingToStars(rating).forEach((s) => stars.add(_buildStatItem(s)));

    return Container(
      height: 60.0,
      margin: EdgeInsets.only(top: 8.0),
      // decoration: BoxDecoration(
      //   color: Color(0xFFEFF4F7),
      // ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: stars,
      ),
    );
  }

  Widget _buildBio(BuildContext context, Collector collector) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'Spectral',
      fontWeight: FontWeight.w400,
      //try changing weight to w500 if not thin
      fontStyle: FontStyle.italic,
      color: Color(0xFF799497),
      fontSize: 16.0,
    );

    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.all(8.0),
      child: Text(
        collector.about,
        textAlign: TextAlign.center,
        style: bioTextStyle,
      ),
    );
  }

  Widget _buildSeparator(Size screenSize) {
    return Container(
      width: screenSize.width / 1.6,
      height: 2.0,
      color: Colors.black54,
      margin: EdgeInsets.only(top: 4.0),
    );
  }

  Widget _buildButtons(Size screenSize) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: 20.0, horizontal: screenSize.width * 0.2),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () => Navigator.pushNamed(context, '/active-requests-collectors'),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
                  border: Border.all(),
                  color: Color(0xFF404A5C),
                ),
                child: Center(
                  child: Text(
                    "AKTIVNI UPITI",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<Collector> getCurrentCollector() async {
    final Auth auth = Provider.of(context).auth;
    CollectorService collectorService = locator<CollectorService>();
    UserService userService = locator<UserService>();

    String fbId = await auth.currentUser();
    User user = await userService.getUserByFbId(fbId);
    return collectorService.getCollectorByUserId(user.userId);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getCurrentCollector(),
        builder: (context, projectSnap) {
          if (projectSnap.connectionState != ConnectionState.done ||
              projectSnap.hasData == null) {
            return CustomLoader();
          }

          Collector collector = projectSnap.data;

          Size screenSize = MediaQuery.of(context).size;
          return Scaffold(
            appBar: BaseAppBar(title: Text('Moj profil'),originalContext: context,appBar: AppBar()),
            body: Stack(
              children: <Widget>[
                _buildCoverImage(screenSize),
                SafeArea(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: screenSize.height * 0.1),
                        _buildProfileImage(screenSize),
                        _buildFullName(collector),
                        _buildUsername(context, collector),
                        SizedBox(height: 8.0),
                        _buildStatContainer(),
                        SizedBox(height: 8.0),
                        _buildBio(context, collector),
                        _buildSeparator(screenSize),
                        _buildPhoneNumber(context, collector),
                        _buildAddress(context, collector),
                        _buildButtons(screenSize),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}