import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:heroji_reciklaze_2020/helpers/auth.dart';
import 'package:heroji_reciklaze_2020/helpers/provider.dart';
import 'package:heroji_reciklaze_2020/models/citizen-model.dart';
import 'package:heroji_reciklaze_2020/models/collector-model.dart';
import 'package:heroji_reciklaze_2020/models/request-model.dart';
import 'package:heroji_reciklaze_2020/models/request-status.dart';
import 'package:heroji_reciklaze_2020/models/user-model.dart';
import 'package:heroji_reciklaze_2020/services/citizen-service.dart';
import 'package:heroji_reciklaze_2020/services/collector-service.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/services/request-service.dart';
import 'package:heroji_reciklaze_2020/services/user-service.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';

class MapPage extends StatefulWidget {
  @override
  MapPageState createState() => MapPageState();
}

class MapPin {
  double lat;
  double lng;
  String title;
  String description;

  MapPin(double lat, double lng, String title, String description) {
    this.lat = lat;
    this.lng = lng;
    this.title = title;
    this.description = description;
  }
}

class MapPageState extends State<MapPage> {
  GoogleMapController mapController;

  final LatLng _center = const LatLng(45.254039, 19.829666);
  final double _zoom = 14.0;

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  Future<Collector> getMyCollector() async {
    Auth auth = Provider.of(context).auth;
    UserService userService = locator<UserService>();
    CollectorService colService = locator<CollectorService>();

    String fbId = await auth.currentUser();
    User user = await userService.getUserByFbId(fbId);
    Collector cit = await colService.getCollectorByUserId(user.userId);

    return cit;
  }

  Future<Stream<QuerySnapshot>> getMyRequests() async {
    RequestService requestService = locator<RequestService>();

    Collector me = await getMyCollector();

    return requestService.getRequestsByCollectorId(me.userId);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getMyRequests(),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done ||
              snapshot.hasData == null) {
            return Container();
          }

          var requestStream = snapshot.data;

          return StreamBuilder(
              stream: requestStream,
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  var requests = snapshot.data.documents
                      .map((doc) => Request.fromMap(doc.data, doc.documentID))
                      .toList();

                  var markers = Set<Marker>();

                  for (int i = 0; i < requests.length; i++) {

                    if(requests[i].locationLat != null && requests[i].locationLng != null) {
                      markers.add(Marker(
                          markerId: MarkerId("id" + i.toString()),
                          position: new LatLng(requests[i].locationLat, requests[i].locationLng),
                          infoWindow: new InfoWindow(
                              title: requests[i].citizenName, snippet: requests[i].type)));
                    }
                  }

                  return Scaffold(
                    appBar: BaseAppBar(
                        appBar: AppBar(),
                        originalContext: context,
                        title: Text('Map page')),
                    body: GoogleMap(
                      markers: markers,
                      onMapCreated: _onMapCreated,
                      initialCameraPosition: CameraPosition(
                        target: _center,
                        zoom: _zoom,
                      ),
                    ),
                  );
                } else {
                  return Container();
                }
              });
        });
  }
}
