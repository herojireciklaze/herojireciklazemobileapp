import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/collectorappdrawer.dart';

class CollectorLandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        appBar: AppBar(),
        originalContext: context,
      ),
      drawer: CollectorAppDrawer(),
      body: Container(
          decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/landing_citizen.jpg'),
            fit: BoxFit.fill),
      )),
    );
  }
}
