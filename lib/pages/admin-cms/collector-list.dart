import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/models/collector-model.dart';
import 'package:heroji_reciklaze_2020/services/collector-service.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/loader.dart';

class CollectorListPage extends StatefulWidget {
  @override
  CollectorListPageState createState() => CollectorListPageState();
}

class CollectorListPageState extends State<CollectorListPage> {

    CollectorService collectorService = locator<CollectorService>();

  Future<List<Collector>> getAllCollectors() async {

    return collectorService.fetchCollectors();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getAllCollectors(),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done ||
              snapshot.hasData == null) {
            return CustomLoader();
          }

          var collectors = snapshot.data;

          return Scaffold(
              appBar: BaseAppBar(
                  appBar: AppBar(),
                  title: Text('Collectors'),
                  originalContext: context),
              body: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  color: Colors.grey[400],
                ),
                itemCount: collectors.length,
                cacheExtent: 50.0,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(collectors[index].fullName),
                  );
                },
              ));
        });
  }
}
