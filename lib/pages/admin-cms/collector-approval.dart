import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/models/collector-model.dart';
import 'package:heroji_reciklaze_2020/services/collector-service.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/widgets/adminappdrawer.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/loader.dart';

class CollectorApprovalPage extends StatefulWidget {
  @override
  CollectorApprovalPageState createState() => CollectorApprovalPageState();
}

class CollectorApprovalPageState extends State<CollectorApprovalPage> {

    CollectorService collectorService = locator<CollectorService>();

  Future<List<Collector>> getPendingCollectors() async {

    return collectorService.getPendingCollectors();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getPendingCollectors(),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done ||
              snapshot.hasData == null) {
            return CustomLoader();
          }

          var collectors = snapshot.data;

          return Scaffold(
              appBar: BaseAppBar(
                  appBar: AppBar(),
                  title: Text('Collectors'),
                  originalContext: context),
              drawer: AdminAppDrawer(),
              body: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  color: Colors.grey[400],
                ),
                itemCount: collectors.length,
                cacheExtent: 50.0,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(collectors[index].fullName),
                    trailing:
                        Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.check_circle_outline),
                        iconSize: 60.0,
                        color: Colors.green,
                        alignment: Alignment.center,
                        autofocus: false,
                        padding: EdgeInsets.all(5),
                        onPressed: ()  {
                          Collector collector = collectors[index];
                          collector.approvedByAdmin = true;
                          collectorService.updateCollectors(collector,collector.collectorId )
                            .then((result)=> {
                              setState(() {})
                            });

                        },
                      ),
                      IconButton(
                        icon: Icon(Icons.clear),
                        iconSize: 60.0,
                        color: Colors.red,
                        padding: EdgeInsets.all(5),
                        autofocus: false,
                        alignment: Alignment.center,
                        onPressed: () => {
                          //Handle collector rejected case
                        },
                      ),
                    ]),
                  );
                },
              ));
        });
  }
}
