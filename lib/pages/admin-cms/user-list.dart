import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/models/citizen-model.dart';
import 'package:heroji_reciklaze_2020/services/citizen-service.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/loader.dart';

class CitizenListPage extends StatefulWidget {
  @override
  CitizenListPageState createState() => CitizenListPageState();
}

class CitizenListPageState extends State<CitizenListPage> {

    CitizenService citizenService = locator<CitizenService>();

  Future<List<Citizen>> getAllCitizens() async {

    return citizenService.fetchCitizens();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getAllCitizens(),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done ||
              snapshot.hasData == null) {
            return CustomLoader();
          }

          var collectors = snapshot.data;

          return Scaffold(
              appBar: BaseAppBar(
                  appBar: AppBar(),
                  title: Text('Citizens'),
                  originalContext: context),
              body: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  color: Colors.grey[400],
                ),
                itemCount: collectors.length,
                cacheExtent: 50.0,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(collectors[index].fullName),
                  );
                },
              ));
        });
  }
}
