import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:heroji_reciklaze_2020/helpers/constants.dart';
import 'package:heroji_reciklaze_2020/models/citizen-model.dart';
import 'package:heroji_reciklaze_2020/services/citizen-service.dart';
import 'package:heroji_reciklaze_2020/services/locator.dart';
import 'package:heroji_reciklaze_2020/widgets/appbar.dart';
import 'package:heroji_reciklaze_2020/widgets/appdrawer.dart';

import '../helpers/auth.dart';
import '../helpers/provider.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _DashboardState createState() => new _DashboardState();
}

class _DashboardState extends State<Dashboard> {

  CitizenService citizenService = locator<CitizenService>();
  List<Citizen> citizens;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {

          Citizen citizen = new Citizen();
          citizen.fullName = "Test Testic";
          citizen.userId = "1";
          citizen.username = "Test";
          citizen.phoneNumber = "123456";
          citizen.about = "hmmm";
          citizen.address = "aa 1";

          citizenService.addCitizen(citizen);
        },
        child: Icon(Icons.add),
      ),
      appBar: BaseAppBar(title: Text(widget.title),originalContext: context,appBar: AppBar()),
      body: Container(
        child: StreamBuilder(
            stream: citizenService.fetchCitizensAsStream(),
            builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                citizens = snapshot.data.documents
                    .map((doc) => Citizen.fromMap(doc.data, doc.documentID))
                    .toList();
                return ListView.builder(
                  itemCount: citizens.length,
                  itemBuilder: (buildContext, index) =>
                  new GestureDetector(
                    onTap: () {
                      //citizenService.removeCitizen(citizens[index].citizenId);
                    },
                    child: Text(citizens[index].fullName ?? ''),
                  ),
                );
              } else {
                return Text('fetching');
              }
            }),
      ),
      drawer: UserAppDrawer(),
    );
  }

      Future<void> signOut() async {
      try {
        Auth auth = Provider.of(context).auth;
        await auth.signOut();
      } catch (e) {
        print(e);
      }
    }
}
