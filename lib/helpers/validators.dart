enum FormType { login, register }

class EmailValidator {
  static String validate(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value) && value.isNotEmpty)
        return 'E-mail nije validan';
      else
        return isEmpty(value);
    }
  }
class PhoneNumberValidator {
  static String validate(String value) {
    Pattern pattern = r'^(?:[0]6)?[0-9]{7,8}$';
    RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value) && value.isNotEmpty)
        return 'Broj telefona nije validan';
      else
        return isEmpty(value);
    }
  }
class PasswordValidator {
  static String validate(String value) {
    if (value.length < 8 && value.isNotEmpty) {
      return 'Šifra mora sadrzati najmanje 8 karaktera';
    } else
      return isEmpty(value);
  }
}

class RequiredFieldsValidator {
  static String validate(String value) {
    return isEmpty(value);
  }
}

String isEmpty(String value) {
  return value.isEmpty ? "Polje ne sme biti prazno" : null;
}
